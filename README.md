# Rhubarb Farmer

A shell script that controls KFS batch jobs, via Rhubarb, all from command line.

# Overview

The idea here, is for te human and the shell to act as scheduler software. Thus, we are kind of like Control-M, but in meat-space. The flow of control is:

1. Human supplies KFS job names text file, and kicks off script in terminal
1. Script executes Rhubarb with needed arguments
1. Rhubarb shuffles files on a common filesystem(FS) between itself and KFS
    * On a dev's machine, this is just the local FS
    * In the UA envs, this is the share drive
1. KFS Batch Invoker watches for the *.run files on the FS, executes jobs, and drops status files each phase
    * Phase I, Initializing: Discover and move Rhubarb's *.run file to a different dir
    * Phase II, Processing: Drop a *.status file, periodically updating contained timestamp each check
    * Pahse III, Completion: Either succes or failure, file contents updated as such, and *.status file moved to 'history' dir
1. Rhubarb sees the status file change to final, and exits with 0 for success, or non-0 for failure (well, or times out w/ error)
1. Script monitors Rhubarb's exit status, and will either:
    * Continue to next job in text file if Rhubarb returned success
    * Or halt, if Rhubrab ever returns a non-0 status
1. Each job in the user-supplied text file is processed this manner, one at a time, until each job is processed

Additionally, this script passes Rhubarb's output all the way through to stdout, as well as adding simple logging too. In such a way, all a user needs to do, is add job names to a text file, launch the script, and each job will be processed
automatically. This means one never needs to go through the UI, clicking many buttons, and constantly refreshing pages, all in order to get multiple jobs processed -- just kick off this script and you're done! This is extremely helpful when trying to iterate quickly in a dev or testing environment.

# Requirements

1. A functioning Rhubarb installation, along with:
    * __PATH__: It is assumed your path has been modifed to include the Rhubarb binaries, a dev's might be:
        * ```export PATH=$PATH:$HOME/git/katt-kfs-rhubarb/bin```
    * Exhaustive steps for Rhubarb installation can be found here:
        * https://confluence.arizona.edu/display/KATT/Installing+RVM%2C+Ruby%2C+and+Rhubarb
1. Access to a Bash shell terminal
    * Tested with Bash v4.3.30(1)-release, on Debian 8.3 (which means Ubuntu and Mint should be fine too)
1. The script must be executed on a machine that has visibility to the common filesystem that Rhubarb and KFS effectively communicate with eachother over
    * For a deveveloper, this means your local filesystem where your KFS instance 'work' directory is
    * For UA instances, this will be the Isilon drive

# Instructions

1. Clone this repository
1. Open a shell, and ```cd``` to the root of the project
1. Create a text file, we'll call it ```job.txt```, but can be called anything, and start adding KFS job names
    * One job name per line
    * ```#``` Comment lines are allowed, and will be ignored (no inline comments, however)
    * Empty lines are allowed, and will be ignored
1. Save the text file, and execute the script: ```$ ./run_batch.sh jobs.txt```
1. Simple looging will be output to stdout and stderr
    1. The script reports start, status, and completion messages
    1. The script also passes through all Rhubarb messages to stdout
1. The script can be used in automated processes, as exit status conventions are followed

