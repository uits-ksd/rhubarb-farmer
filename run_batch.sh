#!/bin/bash
###############################################################################
# run_batch.sh - Run batch jobs via Rhubarb middleware application.
#
# Usage: run_batch.sh <batchJobNamesFile>
#
# The supplied file name should contain valid KFS batch job names, one per line;
# empty and commented lines will be ignored. Each job will be invoked with
# Rhubarb, in the order found in the file. If at any point a job fails, this
# script will report so and then immediately exit.
#
# v0.1.0
# Author: sgskinner
#
# Copyright (C) 2016  The University of Arizona
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################



################################################################################
# Constants
################################################################################
RHUB_EXE="batch_drive" # Rhubarb executable to call
JOBS_FILE="" # List of job names, supplied via CLI arg


################################################################################
# finish() - Perform all cleanup here, and set trap for EXIT
################################################################################
finish() {
    : # no-op
}
trap finish EXIT


###############################################################################
# _usage() - Print required arguments
###############################################################################
_usage() {
    echo "Usage: run_batch.sh [batchJobNamesFile]";
}


################################################################################
# _timestamp()) - Used for log name passed to Rhubarb
################################################################################
_timestamp() {
    date +"%FT%T"
}


###############################################################################
# _test_commented() - If commented, return empty string, else return input
###############################################################################
_test_commented() {
    echo "$1" | grep -v '^\s*#'
}


###############################################################################
# _file_check() - Make sure supplied file is provided and exists, exit if not
###############################################################################
_file_check() {
    if [[ -z "$JOBS_FILE" ]]; then
        >&2 echo "You must supply a batch job-name file arg!"
        >&2 _usage
        exit 1
    elif [[ ! -f "$JOBS_FILE" ]]; then
        >&2 echo "Batch job-name file does not exist: '$JOBS_FILE'"
        >&2 _usage
        exit 1
    fi
}




###############################################################################
# _env_check() - Ensure user has needed ENV set up, if not, exit
###############################################################################
_env_check() {
    #TODO: Possibly set an env in this script, should there ever be a common
    #      env available for techie and BA alike that can be shared
    if [[ -z "$BATCH_HOME" ]]; then
        >&2 echo "The env var BATCH_HOME is not set, Rhubarb can't run without it!" 
        echo "Exiting!"
        exit 1
    elif [[ ! -d "$BATCH_HOME" ]]; then
        >&2 echo "A BATCH_HOME dir does not exist, Rhubarb can't run without it!"
        echo "Exiting!"
        exit 1
    fi
}



###############################################################################
# main() - Driver function
###############################################################################
main () {
    
    # Ensure this user has the needed ENV varibales
    _env_check

    # Set and check our input
    JOBS_FILE="$1"
    _file_check

    # Read each line of supplied file, don't choke if no empty last line
    count=0
    while read -r job_name || [[ -n "$job_name" ]]; do

        # Skip empty or commented lines
        if [[ -z "$job_name" || -z $(_test_commented "$job_name") ]]; then
            continue
        fi
        
        # Let the user know what job is up next
        echo "Calling Rhubarb with job: '$job_name'"

        # Actually call Rhubarb w/ current job_name,
        # e.g.: '$ batch_invoke logfileName job_name
        $RHUB_EXE "${USER}_$(_timestamp)" "$job_name"
        
        # Check exit status of job, report accordingly, stop if failed
        status=$?
        if [[ $status != 0 ]]; then
            >&2 echo "Rhubarb job failed: '$job_name', with exit status of: '$status'!"
            >&2 echo "Exiting!"
            exit 1
        else
            echo "Success!"
        fi
       
        # Bookeeping for final echo
        count=$((count+1))

    done < "$JOBS_FILE"
    
    # All done!
    echo "Completed processing file: '$JOBS_FILE'"
    echo "Total of $count job(s) ran"
    echo "Exiting."
    return 0

}


###############################################################################
main $@

